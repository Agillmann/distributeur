
var amountTotal = 0;
var totalInput = document.getElementById('total');
var codeInput = document.getElementById('code');

var pos = '';

var products = [
    {
        name: 'Coca-Cola',
        price: 1.10,
        code: '10'
    },
    {
        name: 'Fanta',
        price: 1.05,
        code: '21'
    },
    {
        name: 'Salade',
        price: 0.90,
        code: '34'
    },
    {
        name: 'Bonbons',
        price: 2.30,
        code: '42'
    },
];

// Ajoute de l'argent au distributeur
function addEuro(amount) {
    amountTotal += amount;
    setInputValue();
}

// Fonction pour modifier la valeur du champ texte
function setInputValue() {
    // Ajout du round pour un affichage plus propre dans le champs texte
    totalInput.value = Math.round( amountTotal * 1000 ) / 1000;
}

// Ajoute un nombre pour la position
function selectPos(number) {
    pos += number;
    codeInput.value += number;
    console.log(pos);
    // Dés qu'on a la position à deux chiffres on cherche le produit
    if (pos.length === 2) {
        searchProduct();
        codeInput.value = "";
    }
}

// On cherche le produit
function searchProduct() {
    for (var p = 0; p < products.length; p++) {
        var product = products[p];
        if (product.code === pos) {
            if (amountTotal >= product.price) {
                amountTotal -= product.price;
                pos = '';
                setInputValue();
                alert(product.name);
                // Pas besoin de continuer, on a trouvé notre produit
                return;
            } else {
                pos = '';
                alert('Veuillez ajouter de l\'argent');
                // Pas besoin de continuer, on a pas assez d'argent
                return;
            }
        }
    }

    // Le produit n'a pas été trouvé
    pos = '';
    alert('Produit inexistant');
}
